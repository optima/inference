## Installation guide of all software on the Adlink NEON smart-camera
Password Adlink (for sudo): adlink <br/>
This installation guide is tested on: Jetpack 4.4 (R32.4.3), CUDA 10.2, Python 3.6, TensorRT 7.1.3.1 <br/>

There are several libraries already installed on the Adlink Neon: <br/>
python2, python3, opencv, cuda, numpy <br/> <br/>

**1) Check the versions of the installed packages (in the terminal):** 
- cat /etc/nv_tegra_release *(should print the version of Jetpack)*
- nvcc --version *(should the CUDA details)* <br/> <br/>

**2) Install Pytorch and Torchvision:** 
<br/>
First, check which Pytorch version is compatible with the Jetpack/Cuda version. In this case it's Pytorch version 1.6 (you can Google the compatibility at this URL: https://forums.developer.nvidia.com/t/pytorch-for-jetson-version-1-7-0-now-available/72048) <br/>

Run the following commands in the terminal:
- sudo apt install python3-pip
- pip3 install wheel
- wget https://nvidia.box.com/shared/static/9eptse6jyly1ggt9axbja2yrmj6pbarc.whl -O torch-1.6.0-cp36-cp36m-linux_aarch64.whl
- sudo apt-get install python3-pip libopenblas-base libopenmpi-dev 
- pip3 install Cython
- pip3 install numpy torch-1.6.0-cp36-cp36m-linux_aarch64.whl
- sudo apt-get install libjpeg-dev zlib1g-dev libpython3-dev libavcodec-dev libavformat-dev libswscale-dev
- git clone --branch v0.7.0 https://github.com/pytorch/vision torchvision
- cd torchvision
- export BUILD_VERSION=0.7.0
- sudo python3 setup.py install (if this doesn't work: python3 setup.py install)
- cd ../ <br/> <br/>

**3) Check if Pytorch and Torchvision are correctly installed:** 
- python3
- import torch
- import torchvision
- print(torch.version.cuda) *(should print: 10.2)*
- print('CUDA available: ' + str(torch.cuda.is_available())) *(should print: CUDA available: True)*
- torch.cuda.get_device_name(0) *(should print: NVIDIA Tegra X2)*
- quit() <br/> <br/>

**4) Install the software packages needed for YOLOv5:**
- pip3 install matplotlib
- pip3 install pycocotools
- pip3 install tqdm
- pip3 install PyYAML==5.3.1 
- pip3 install pycuda 
- sudo apt-get install python3-tk <br/> <br/>

**5) Clone the optima/inference code:**
- git clone https://git.wur.nl/optima/inference.git
- cd inference <br/> <br/>

**6) Install the software packages needed for classification (EfficientNet-Pytorch):**
- wget https://nvidia.box.com/shared/static/49fzcqa1g4oblwxr3ikmuvhuaprqyxb7.whl -O onnxruntime_gpu-1.6.0-cp36-cp36m-linux_aarch64.whl
- pip3 install onnxruntime_gpu-1.6.0-cp36-cp36m-linux_aarch64.whl <br/> <br/>

**7) Install the SWIG software (needed for image acquisition):**
- download swig-3.0.12: https://sourceforge.net/p/swig/news/2017/01/swig-3012-released/
- cd swig-3.0.12
- ./configure --prefix=/home/adlink/library/swigtool
- sudo make
- sudo make install
- sudo gedit /etc/profile
- copy-paste these two lines at the end of the file: <br/>
&nbsp;&nbsp;&nbsp;export SWIG_PATH=/home/adlink/library/swigtool/bin <br/>
&nbsp;&nbsp;&nbsp;export PATH=$SWIG_PATH:$PATH <br/>
- source /etc/profile
- sudo reboot
- swig -version *(should print: 3.0.12)* <br/> <br/>

**8) Install pypylon software (needed for image acquisition):**
- git clone https://github.com/basler/pypylon.git
- cd pypylon
- pip3 install .
- cd samples
- python3 opencv.py <br/> <br/>

**9) Install the GPS software:**
- pip3 install pyserial
- pip3 install pynmea2
- sudo usermod -a -G tty $USER
- sudo usermod -a -G dialout $USER
- sudo reboot <br/> <br/>

**10) OPTIONAL: Install the TensorRT backend for ONNX (to significantly speed up the EfficientNet image classification):**
<br/> The description below has been tested for TensorRT version 7.1.3.1. Be aware that this installation requires the presence of the SWIG software (step 7)!
- dpkg -l | grep TensorRT *(should print a big list with all TensorRT libraries, including versions)*
- sudo apt-get install python3-pip libprotoc-dev protobuf-compiler
- pip3 install onnx==1.6.0 
- cd inference/onnx-tensorrt
- sudo python3 setup.py install <br/>

If there's still an error, like **unable to execute 'swig': No such file or directory**, then you have to have to run the following command:
- swig -python -c++ -modern -builtin -o nv_onnx_parser_bindings_wrap.cpp nv_onnx_parser_bindings.i (*should execute without any feedbacks and errors*)
- sudo python3 setup_2.py install <br/> <br/>

**11) OPTIONAL: Make desktop shortcuts:**
- follow the instructions in this URL: https://gist.github.com/nathakits/7efb09812902b533999bda6793c5e872
- chmod u+x /home/adlink/inference/inference.py
- Right click on the desktop-file and select Properties. In the Permissions tab, set the execute to allow executing file as program

[Desktop Entry]<br/>
Name=detection<br/>
Version=1<br/>
Icon=/home/adlink/inference/data/optima.jpg<br/>
Exec=python3 /home/adlink/inference/inference.py --cfg /home/adlink/inference/cfg/detection.yaml<br/>
Terminal=true<br/>
Type=Application<br/>

[Desktop Entry]<br/>
Name=classification<br/>
Version=1<br/>
Icon=/home/adlink/inference/data/optima.jpg<br/>
Exec=python3 /home/adlink/inference/inference.py --cfg /home/adlink/inference/cfg/classification.yaml<br/>
Terminal=true<br/>
Type=Application<br/>

[Desktop Entry]<br/>
Name=classification_patches<br/>
Version=1<br/>
Icon=/home/adlink/inference/data/optima.jpg<br/>
Exec=python3 /home/adlink/inference/inference.py --cfg /home/adlink/inference/cfg/classification_patches.yaml<br/>
Terminal=true<br/>
Type=Application<br/>

[Desktop Entry]<br/>
Name=keyboard<br/>
Version=1<br/>
Icon=/home/adlink/inference/data/keyboard.jpg<br/>
Exec=onboard<br/>
Terminal=false<br/>
Type=Application<br/>


