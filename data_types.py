import json
from datetime import datetime
from types import SimpleNamespace as Namespace
from sys import maxsize
#https://medium.com/@yzhong.cs/serialize-and-deserialize-complex-json-in-python-205ecc636caa 


##-----------------------------------------------------------------------------------------------------------
#AGENSO DATACLASSES

#disease class
class disease():
    def __init__(self, prob, hits):
        self.prob = prob
        self.hits = hits    

#data object class
class data_class():#lat, lng, hdop, satnum, gps_quality, timestamp
    def __init__(self):
        self.timestamp = "0-0-0T0:0:0"
        self.gps_quality = "Not valid" #values: Not valid, Fixed, Diff fix, RTK, RTK F
        self.lat = maxsize
        self.lng = maxsize
        self.hdop = 0
        self.satnum = 0
        self.diseases = []

    def set_gpsquality(self, quality_indicator):
        if quality_indicator == 1:
            self.gps_quality = "fixed"
        elif quality_indicator == 2:
            self.gps_quality = "dif_fix"
        elif quality_indicator == 4:
            self.gps_quality = "rtk"
        elif quality_indicator == 5:
            self.gps_quality = "rtk_f"
        else: #0 and undefined values
            self.gps_quality = "not_valid" 

#bulk upload data class
# class batch_data_class():
#     def __init__(self):
#         self.items = []

def batch_tofile(data, filename):
    #serialise the class as list of dicts
    with open(filename, 'w') as file:
        json.dump([ob.__dict__ for ob in data], file, default = lambda o: o.__dict__, indent = 4)    

# def data_tojson(data):
#     #serialise the class as dicts
#     data_json = json.dumps(data.__dict__, default = lambda o: o.__dict__, indent = 4)
#     return data_json

def data_tofile(data, filename):
    #serialise the class as dicts
    with open(filename, 'w') as file:
        json.dump(data, file, default = lambda o: o.__dict__, indent = 4)


# reads the json to a dict, not to a class instance! 
# Acces as data_obj['timestamp'] instead of data_obj.timestamp
# def data_fromjson(data_json):
#     data = json.loads(data_json)
#     return data

# #reads the json into a general class object. you can use it as a normal class instance. 
# def data_fromfile(filename):
#     with open(filename) as file:
#         data = json.load(file, object_hook= lambda d: Namespace(**d))
#     return data

#read file as json string 
def batch_fromfile(filename):
    try:
        with open(filename) as file:
            data = json.load(file)
    except Exception as e:
        print(f"no data in file: {filename}, error: {str(e)}")
        return (None, False)
    return (data, True)   

##-----------------------------------------------------------------------------------------------------------
#GPS DATA class
class timestep:
    def __init__(self, nmea_GGA_msg, latdif, lngdif): 
        self.lat = nmea_GGA_msg.latitude
        self.lng = nmea_GGA_msg.longitude
        self.latdif = latdif #dif compared to previos value (+/-diff per second)
        self.lngdif  = lngdif #dif compared to previos value (+/- diff per second)
        self.hdop = nmea_GGA_msg.horizontal_dil # horizontal error of the GPS coordinates
        self.satnum = nmea_GGA_msg.num_sats
        self.gpsquality = nmea_GGA_msg.gps_qual
        self.gpstime = nmea_GGA_msg.timestamp.strftime("%y-%m-%dT%H:%M:%S.%f")
        self.rectime = datetime.utcnow().strftime("%y-%m-%dT%H:%M:%S.%f")
        self.moving = False

##-----------------------------------------------------------------------------------------------------------
#ring buffer of gps values so we can retreive them & determine if we are moving
class GPS_Ringbuffer:
    def __init__(self, size):
        self.min_movement_frame = 0 #minimal movement vetween frames
        self.min_movement = 0 #minimal movement over the entire buffer. 
        self.decdegr_to_m = 0.00001 #very rough conversion from DD to M
        self.size = size
        self.prevlat = maxsize
        self.prevlng = maxsize
        self.prevtimestamp = datetime.utcnow()#.strftime("%y-%m-%dT%H:%M:%S.%f")
        self.data = []
        self.f_pnt = 0 #inital poisition
        self.l_pnt = 1 #last position (always before the firstpo in a ring)
        #init array
        fake_nmea_GGA_msg = fake_nmea_class(self.prevtimestamp)
        for _ in range(0, size):#i
            self.data.append(timestep(fake_nmea_GGA_msg, 0,0))

    def update_pointer(self, pointer):
        return (pointer + 1) % self.size
        
    def set_min_frame_movement(self, min_movement_pulse):
        self.min_movement = min_movement_pulse * self.decdegr_to_m * self.size
        self.min_movement_frame = min_movement_pulse * self.decdegr_to_m

    #put new value in the rinngbuffer en update the values
    def put(self, nmea_GGA_msg, timestamp):
        self.f_pnt = self.update_pointer(self.f_pnt)
        self.l_pnt = self.update_pointer(self.l_pnt)
        lat = nmea_GGA_msg.latitude
        lng = nmea_GGA_msg.longitude
        time_dif = (timestamp - self.prevtimestamp).total_seconds()
        lat_dif = (lat - self.prevlat)
        lng_dif = (lng - self.prevlng)

        self.data[self.f_pnt].lat = lat
        self.data[self.f_pnt].lng =lng
        self.data[self.f_pnt].latdif = lat_dif/time_dif 
        self.data[self.f_pnt].lngdif = lng_dif/time_dif 
        self.data[self.f_pnt].hdop = nmea_GGA_msg.horizontal_dil
        self.data[self.f_pnt].satnum = nmea_GGA_msg.num_sats
        self.data[self.f_pnt].gpsquality = nmea_GGA_msg.gps_qual
        self.data[self.f_pnt].gpstime = nmea_GGA_msg.timestamp.strftime("%y-%m-%dT%H:%M:%S.%f")
        self.data[self.f_pnt].rectime = timestamp.strftime("%y-%m-%dT%H:%M:%S.%f")
        self.data[self.f_pnt].moving = self.determine_hasmoved()
        #print('prev_lat: %f, lat: %f, diff: %f' %(self.prevlat, lat, self.data[self.f_pnt].latdif))
        #print('prev_lng: %f, lng: %f, diff: %f' %(self.prevlng, lng, self.data[self.f_pnt].lngdif))
        self.prevtimestamp = timestamp
        self.prevlat = lat
        self.prevlng = lng
        
    #compute the current location based on the time and position difference between the last two coordinates
    def get_currentlocation(self, timestamp):
        avg_latdiff = self.data[self.f_pnt].latdif
        avg_lngdiff = self.data[self.f_pnt].lngdif
        
        time_dif = (timestamp - self.prevtimestamp).total_seconds()
        #print(avg_latdiff)
        #print(avg_lngdiff)
        #print(time_dif)
        lat = self.prevlat + avg_latdiff * time_dif
        lng = self.prevlng + avg_lngdiff * time_dif
        return (lat, lng)
    
    #return current GPS location as AGESO data class
    def get_dataobject(self, timestamp):
        data_obj = data_class()
        data_obj.timestamp = timestamp.strftime("%y-%m-%dT%H:%M:%S")
        data_obj.set_gpsquality(self.data[self.f_pnt].gpsquality)
        (data_obj.lat, data_obj.lng) = self.get_currentlocation(timestamp)
        data_obj.hdop = float(self.data[self.f_pnt].hdop)
        data_obj.satnum = int(self.data[self.f_pnt].satnum)
        return (data_obj, self.data[self.f_pnt].moving, self.data[self.f_pnt].rectime )

    #determine movement over the last samples
    def determine_hasmoved(self):
        total_latdiff = 0
        total_lngdiff = 0

        for pos in self.data:
            total_latdiff += pos.latdif
            total_lngdiff += pos.lngdif 
        total_movement = abs(total_latdiff + total_lngdiff)
        if (total_movement > self.min_movement):
            return True
        else:
            return False

    #only use first & last sample, faster for big buffers
    def determine_hasmoved_fast(self):
        itemone = self.data[self.f_pnt]
        itemtwo = self.data[self.l_pnt]

        lat_movement = abs(itemone.lat - itemtwo.lat)
        lng_movement = abs(itemone.lng - itemtwo.lng)
        total_movement = abs(lat_movement + lng_movement)
        if (total_movement > self.min_movement):
            return True
        else:
            return False        


    def determine_has_moved_onlynow(self):
        total_movement = self.data[self.f_pnt].latdif + self.data[self.f_pnt].lngdif 
        if (total_movement > self.min_movement_frame):
            return True
        else:
            return False  


    #print the buffer for debugging purposes
    def print_all_values(self):
        for item in self.data:
            print('item, latdif: %f, lngdif: %f' %(item.latdif, item.lngdif))

    #return last item as string
    def curitem_as_string(self, camera_timestamp):
        item = self.data[self.f_pnt]
        GPS_string = str(item.lat) + "," + str(item.lng) + "," + str(item.latdif) + "," + str(item.lngdif) + "," 
        GPS_string += str(item.hdop) + "," + str(item.satnum) + "," + str(item.gpsquality) + "," 
        GPS_string += item.gpstime + "," + item.rectime + "," + camera_timestamp.strftime("%y-%m-%dT%H:%M:%S.%f") + "," + str(item.moving) +"\n"
        return GPS_string



#fake nmea class, used to initialise the RING buffer
class fake_nmea_class:
    def __init__(self,prevtimestamp): 
        self.latitude = maxsize
        self.longitude = maxsize
        self.horizontal_dil = 0
        self.num_sats = 0
        self.gps_qual = 0
        self.timestamp = prevtimestamp


if __name__ == '__main__':
    fnc = fake_nmea_class(datetime.now())
    gps_test = GPS_Ringbuffer(5)
    gps_test.set_min_frame_movement(0.1)
    lat_init = 52.64538985
    lon_init = 5.667874878

    for i in range(6):
        fnc.latitude = lat_init + (i * 1e-6)
        fnc.longitude = lon_init + (i * 1e-6)
        gps_test.put(fnc, datetime.now())

    (obj, is_moving) = gps_test.get_dataobject(datetime.now())
    print(is_moving)
