# @Author: Pieter Blok
# @Date:   2021-04-15 17:20:35
# @Last Modified by:   Pieter Blok
# @Last Modified time: 2021-04-15 17:20:58
import os 
import cv2
from datetime import datetime
from threading import Thread
from pypylon import pylon #camera
from itertools import cycle
from glob import glob
from sys import maxsize

from global_vars import *
from usb_Filestorage import *

#thread that takes images and stores the image & GPS data on the HDD.
class take_img(Thread):
    def __init__(self, CONFIG):
        self._running = True
        self.save_IMGS = CONFIG['save-IMGS']
        self.save_GPS = CONFIG['save-GPS']
        self.usb = USB_class(CONFIG['save-every-x'])
        self.GPS_check = CONFIG['GPS-checks']
        # connecting to the first available camera
        self.camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())

        # Grabing Continusely (video) with minimal delay
        self.camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly) 
        self.converter = pylon.ImageFormatConverter()

        # converting to opencv bgr format
        self.converter.OutputPixelFormat = pylon.PixelType_BGR8packed
        self.converter.OutputBitAlignment = pylon.OutputBitAlignment_MsbAligned

        Thread.__init__(self)
        print("Image taker thread started")

    def run(self):     
        global GPS_lock, GPS_ringbuf, to_process_deque, to_display_queue
        while self.camera.IsGrabbing() and self._running:
            usbgb = self.usb.free_USBGBs
            freememory = free_gbs()
            grabResult = self.camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
            if grabResult.GrabSucceeded():
                timestamp = datetime.utcnow()
                filename = timestamp.strftime("%y_%m_%dT%H_%M_%S_%f")
                image = self.converter.Convert(grabResult)
                img = image.GetArray() 
                with GPS_lock:
                    (data_obj, is_moving, gps_rectime) = GPS_ringbuf.get_dataobject(timestamp)
                    to_display_deque.append({'data': data_obj, 'free_mem': freememory, 'free_mem_hdd': usbgb, 'is_moving':is_moving, 'gps_rectime': gps_rectime})               

                    #save GPS data
                    if (self.GPS_check == False) or (self.save_GPS == True) and (is_moving == True):
                        gps_filename = timestamp.strftime("%y_%m_%d") #one file/day
                        self.usb.write_line(GPS_ringbuf.curitem_as_string(timestamp), gps_filename)

                #only process images when we are moving & the GPS buffer is filled
                if (self.GPS_check == False) or (is_moving == True):
                    if (self.GPS_check == False) or ((data_obj.lat < maxsize) and (data_obj.lng < maxsize)):
                        #send image to neural network
                        to_process_deque.append({'file_name': filename, 'img': img,'data': data_obj})
                    if self.save_IMGS == True:
                        self.usb.save_img(img, timestamp.strftime("%y_%m_%dT%H"), filename)
            grabResult.Release()
        self.camera.StopGrabbing()

    def terminate(self):
        self._running = False



class read_img(Thread):
    def __init__(self, CONFIG):
        self._running = True
        # Initialize the class specific parameters
        self.imagesRgb = cycle(sorted(glob(os.path.join(CONFIG['img-input-folder'], '*.jpg'))))
        print("Image loader thread started")
        Thread.__init__(self)

    def run(self):
        while self._running:          
            image_file = next(self.imagesRgb) #does not pick new images
            base_filepath = image_file[:-4] 
            base_filename = base_filepath.rsplit('/',1)[-1]
            img = cv2.imread(image_file, cv2.IMREAD_COLOR)
            print("image read: " + image_file)
            with GPS_lock:
                timestamp = datetime.utcnow()
                (data_obj, is_moving, gps_rectime) = GPS_ringbuf.get_dataobject(timestamp)
                #send image to neural network
                to_process_queue.put({'file_name': base_filename, 'img': img,'data': data_obj})#blocks when the queue is full

    def terminate(self):
        self._running = False