import subprocess
import os
import cv2

MOUNT_DIR = "/mnt/usb_stick"
DISKTYPE = 'sd'#check which name the disk gets, for me they always started with 'sd'
DISKPREFIX = "/dev/"
USER = 'adlink'

GPS_folder = 'gps'
IMG_folder = 'imgs'
IMG_ext = '.png'

#important notes, when the USB drive is connected when the device is turned on it is mounted as root!
#the user only has read permission... 
#unmount & remount to get read/write permission. 
#when the disk is pulled out unexpectedly it stays mounted, but write operations give errors
# 'ls-l /dev/disk/by-uuid' can be run to check if the device is disconnected. 


class USB_class:
    def __init__(self, save_frequency):
        self.UUID = None #UUID of the disk
        self.reconnect()
        self.free_USBGBs = self.check_free_diskspace()
        self.writeIMg_cnt = 0 #counter to avoid checking diskspace for every call
        self.writeGPS_cnt = 0
        self.save_frequency = save_frequency

    #need sudo rights for these folders...
    def check_direxists(self, dir):
        if dir != None:
            if not os.path.exists(dir):
                #os.makedirs(dir) #need sudo rights
                create_dircmd = ["sudo", "mkdir", dir]
                process = subprocess.Popen(create_dircmd, stderr = subprocess.PIPE)
                error = process.stderr.read().decode()
                if error != '':
                    print("create directory error: %s" %error)
                    return False
            return True

    #get free gb's on disk
    def check_free_diskspace(self):
        if self.UUID == None:
            return 0
        else:#df | grep ^/dev/sdc1
            get_memory = "df | grep "+DISKPREFIX + self.UUID
            process = subprocess.Popen(get_memory, stdout= subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
            error = process.stderr.read().decode()
            if error != '':
                print("get free diskspace error: %s" %error)
                return 0
            output = process.stdout.read().decode()
            #used_memory = output.split()[2]
            avail_memory =  output.split()[3]
            return (float(avail_memory)/1000000) #gb's


    #try to find & connect to USB
    def reconnect(self): 
        prev_UUID = self.UUID
        self.UUID = self.find_disk()
        if (prev_UUID != None) and (self.UUID != prev_UUID):
            self.umount(MOUNT_DIR)
            print('disk disconnected')
            return False
        elif self.UUID == None:
            print('no disk found')
            return False

        if self.check_mounted():
            #if mounted, check if folders exist
            IMG_dir = os.path.join(MOUNT_DIR, IMG_folder)
            GPS_dir = os.path.join(MOUNT_DIR, GPS_folder)
            self.check_direxists(IMG_dir)
            self.check_direxists(GPS_dir)
            return True
        else:
            print('error: failed to mount disk')
        return False

    # check if a disk of the given type is connected to the device
    def find_disk(self):
        find_dev_UUID = ["ls","-l", "/dev/disk/by-uuid/"]
        process = subprocess.Popen(find_dev_UUID, stdout= subprocess.PIPE, stderr = subprocess.PIPE)
        error = process.stderr.read().decode() 
        if error != '':
            print("checkdisk error: %s" %error)
        output = process.stdout.read().decode()

        for line in output.splitlines():
            device = line.split('>')
            if len(device) == 2:
                device_UUID = device[1]
                #find returns -1 if the device is not found
                if device_UUID.find(DISKTYPE) != -1:
                    UUID = device_UUID.split('/')[-1]
                    #print('found drive %s' %UUID)
                    return UUID
        return None


    # returns true if the disk is already mounted
    def check_mounted(self):
        process = subprocess.Popen(["df"], stdout= subprocess.PIPE, stderr = subprocess.PIPE)
        error = process.stderr.read().decode()
        if error  != '':
            print("mounting error: %s" %error)
        output = process.stdout.read().decode()

        check_UUID = DISKPREFIX + self.UUID
        for idx, mntline in enumerate(output.splitlines()):
            if idx == 0:
                continue
            found_name = mntline.split()[0]
            found_mnt =  mntline.split()[5]
            if(found_name == check_UUID):
                if (found_mnt == MOUNT_DIR):
                    return True #already mounted correctly
                else: #mounted by root at boot: remount
                    if self.umount(found_mnt):
                        if self.mount_disk():
                            return True
        return False #not mounted

    # umount disk
    def umount(self, mnt_name):
        Umount = ["sudo","umount", mnt_name]
        process = subprocess.Popen(Umount, stderr = subprocess.PIPE)
        error = process.stderr.read().decode()
        if error != '':
            print("umount error: " + error)
            return False
        return True


    # mount disk
    def mount_disk(self):
        #vfat for fat 16 & fat32 drives, default user id =1000
        Mount = ["sudo","mount", "-t", "ntfs", (DISKPREFIX + self.UUID), MOUNT_DIR,"-o", "rw,uid=1000,gid=1000,umask=133,dmask=022"]
        process = subprocess.Popen(Mount, stderr = subprocess.PIPE)
        error = process.stderr.read().decode()
        if error  != '':
            print("mount error: " + error)
            return False
        return True


#------------------------------------------
#program handles
    def save_img(self, img, subfolder, filename):
        if self.UUID == None:
            if not self.reconnect():
                return
        file_dir = os.path.join(MOUNT_DIR, IMG_folder, subfolder)
        self.writeIMg_cnt = self.writeIMg_cnt  + 1
        try:
            #avoid overflowing the disk, we save one/x files and check the diskspace every 50 files.
            if self.writeIMg_cnt  %(self.save_frequency*50) == 0:
                self.free_USBGBs = self.check_free_diskspace()
                self.writeIMg_cnt = 0
            if self.free_USBGBs < 0.1: 
                return
            elif self.writeIMg_cnt  % self.save_frequency == 0:
                #create subfolder if it not there yet & save image
                if not self.check_direxists(file_dir):
                    raise Exception("disk not connected")
                file_path = os.path.join(file_dir, (filename + IMG_ext))
                cv2.imwrite(file_path, img)
                return
        except Exception as e:
            if not self.reconnect():
                print("USB write img error: %s" %str(e))
                return


    #write a line to the GPS logfile
    def write_line(self, data_string, filename):
        if self.UUID == None:
            if not self.reconnect():
                return
        self.writeGPS_cnt = self.writeGPS_cnt  + 1
        try:
            #avoid overflowing the disk
            if self.writeGPS_cnt  %(self.save_frequency*50) == 0:
                self.free_USBGBs = self.check_free_diskspace()
                self.writeGPS_cnt = 0
            if self.free_USBGBs < 0.1: 
                return
            elif self.writeGPS_cnt  % self.save_frequency ==0:
                with open(os.path.join(MOUNT_DIR, GPS_folder, (filename + ".txt")), "a") as file:
                    file.write(data_string)
        except Exception as e:
            if not self.reconnect():
                print("USB write img error: %s" %str(e))
                return