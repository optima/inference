# Inference source-code for the Adlink Neon smart-camera

## Installation
See [Install_Guide_Adlink_Neon.md](Install_Guide_Adlink_Neon.md)
<br/> <br/>

## Getting started
First copy-paste the ONNX weight-files into the folder that belongs to the specific use-case:
- weights/apple_scab
- weights/downy_mildew
- weights/alternaria
<br/> <br/>

Copy-paste both the **_efficientnet_bs1.onnx** and **_efficientnet_bs6.onnx** so that we can choose whether to analyse on the single image (bs1) or on the single image including 
five cropped (zoomed) patches of the image (bs6). 
<br/> <br/>

## Image inference with the smart-camera
1. Open a new terminal.
2. Go to the root-folder (cd inference)
3. Run the python file **inference.py**, using the appropriate arguments (see below): <br/> <br/>

| Argument        	| Description           												|
| ----------------------|-----------------------------------------------------------------------------------------------------------------------|
| --dl-mode	        | Specify the deep-learning mode. Choose between **detection** or **classification**					|
| --img-patches		| Give this command when you want to analyse the image including five cropped (zoomed) patches of the image		|
| --use-case     	| What use-case are you working on? Choose **apple_scab** or **downy_mildew** or **alternaria**				|
| --img-size		| The input image-size for the deep-learning algorithm. 								|
| --max-img-width	| Optional: when img-patches is activated, specify the desired maximum width to transform the image to			|
| --max-img-height	| Optional: when img-patches is activated, specify the desired maximum height to transform the image to			|
| --source	 	| Optional: input folder that contains the images to read   								|
| --weightsfolder	| Folder with the weight-files. Default is **weights**    								|
| --device	 	| Specify the computing device. Choose between **CUDA** or **cpu**							|
| --conf-thres	 	| Confidence-threshold for the detection algorithm 									|
| --nms-thres	 	| IOU threshold for non-maximum suppression for the detection algorithm 						|
| --output	 	| Neural net output folder. Default is **results/adlink_acquistion_session1** 						|
| --view-img	 	| Give this command when you want to display the images and the detections/classes					|
| --logfile-folder 	| Folder save the logfiles in. Default is **data/temp**									|
| --img-folder	 	| Folder to save image in. Default is **data/temp/img**									|
| --data-folder	 	| Folder to save the GPS data in. Default is **data/temp/GPSdata**							|
| --tosend-folder 	| Folder to save unsend jsons in. Default is **data/temp/tosend**							|
| --badreq-folder 	| Folder to save jsons that give errors in. Default is **data/temp/badreq**						|
| --debug-mode	 	| Give this command when you want to read images from **source** instead of camera					|
| --disable-data 	| Give this command when you want to disable the reading of the txt detections files					|
| --nn-only	 	| Give this command when you want to only run neural network with images from the **source** for testing purposes	|
| --tensorrt	 	| Give this command when you want to activate TensorRT (to speed-up the algorithm)					|

<br/>
Example: 
<br/> python inference.py --dl-mode detection --use-case apple_scab --img-size 640 --conf-thres 0.4 --nms-thres 0.3 --view-img --tensorrt<br/> <br/> <br/> 


## Image inference with images loaded from disk (for testing purposes)
1. Open a new terminal.
2. Go to the annotation folder (cd inference)
3. Run the python file **inference.py**, using the appropriate arguments (see above) <br/> 

Example: 
<br/> python inference.py --dl-mode classification --img-patches --use-case apple_scab --img-size 545 --source data/images --view-img --debug-mode --disable-data --nn-only --tensorrt<br/> <br/> <br/> 


## Contribution
This source-code was developed for the H2020 OPTIMA project. 
<br/> <br/>
Please contact us when you have some questions:
- Tim van Daalen (tim.vandaalen@wur.nl)
- Pieter Blok (pieter.blok@wur.nl)
