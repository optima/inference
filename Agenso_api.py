
import requests
import json

credentials_filepath = "/home/adlink/inference/credentials.json"
login_url = "https://dss.optima-h2020.eu/api/v1/login"
upload_url = "https://dss.optima-h2020.eu/api/v1/sightings"
bulk_upload_url = "https://dss.optima-h2020.eu/api/v1/sightings/bulk"
retreive_url = "https://dss.optima-h2020.eu/api/v1/sightings"

#Login to the AGENSO api to get the token
class AGENSO:
    def __init__(self):
        self.token = None 
        self.login()

    def login(self):
        #get email & password from file (this file is not uploaded to the git)
        with open(credentials_filepath) as f:
            credentials = json.load(f)
            #print(credentials)

        #send message
        data = {'email':credentials['email'], 'password': credentials['password']}
        headers = {'Accept': 'application/json'}
        try:
            r = requests.post(login_url, data = data, headers = headers)
            if r.status_code != 200: #Http Error
                print("URL: %s, statuscode: %d, reply: %s" %(login_url, r.status_code, r.text))
                self.token = None
            else :                   
                self.token = json.loads(r.text)["token"]

        except Exception as e:
            self.token = None 
            if not type(e).__name__ == "ConnectionError":
                print("agenso login error: %s"  %str(e))

    #send the json to AGENSO
    def upload(self, json_data):
        if self.token == None:
            self.login()
        url = upload_url
        json_string = json.dumps(json_data)
        (status_code, reply) = self.upload_helper(url, json_string)
        if status_code != 200:
            self.token = None
            return (False, reply)
        return (True, reply)

    def batch_upload(self, json_string):
        if self.token == None:
            self.login()
        url = bulk_upload_url
        (status_code, reply) = self.upload_helper(url, json_string)
        if status_code != 200:
            self.token = None
            return (False, reply)
        return (True, reply)

    #handles both normal and bluk uploads
    #for genral message: status 200 if succesful
    #for bulk also: [[true,200],[true,200]] (for each subrequest)
    def upload_helper(self, url, json_string):
        params = {'api_token' : self.token}
        headers = {'Content-Type': 'application/json'}
        try:
            r = requests.post(url, params= params, headers = headers, data=json_string)
            if (r.status_code != 200): #Http Error, resturn status: False
                print("URL: %s, statuscode: %d, reply: %s" %(url, r.status_code, r.text))
                status = "error"
            else:
                status = json.loads(r.text)
            return (r.status_code, status)
        except Exception as e:
            print("Agenso error" + str(e))
            #if type(e).__name__ == "ConnectionError":
            return (0, 0)


    #get data from AGENSO for a specific location/timestamp
    #params = {"coordinates.latitude": 51.969189, "coordinates.longitude": 5.665395}
    #{"timestamp": 2020-07-02T11:53:50}
    def retreive(self, params):
        result = ""
        params['api_token'] = self.token
        r = requests.get(retreive_url, params= params)
        if (r.status_code != 200): #Http Error
            print("URL: %s, statuscode: %d, reply: %s" %(retreive_url, r.status_code, r.text))
        else:
            result = r.text
        return result


if __name__ == '__main__':
    #login
    agenso = AGENSO()
    
    #upload data
    test_data = {
    "timestamp": "2020-07-02T11:53:50",
    "gps_quality": "fixed",
    "hdop": 1.4,
    "satnum": 5,
    "lat": 5.969189,
    "lng": 5.665395,
    "diseases": [
        {
            "downy_mildew": {
                "prob": 1.00,
                "hits": 10
            },
            "alternaria": {
                "prob": 0.333,
                "hits": 5
            }
        }
    ]
}
    data_json = json.dumps(test_data, sort_keys=True, indent=4)
    status = agenso.upload( data_json)
    print(status)
    agenso.retreive( {"coordinates.latitude": 51.969189, "coordinates.longitude": 5.665395})
