import os
from PIL import Image
import torch
import torchvision.transforms as transforms
import json
import numpy as np
import onnx
import onnx_tensorrt.backend as backend
import time

np.set_printoptions(formatter={'float_kind':'{:f}'.format})

supported_cv2_formats = (".bmp", ".dib", ".jpeg", ".jpg", ".jpe", ".jp2", ".png", ".pbm", ".pgm", ".ppm", ".sr", ".ras", ".tiff", ".tif")

def list_images(imagedir):
    if os.path.isdir(imagedir):
        all_files = os.listdir(imagedir)
        images = [x for x in all_files if x.lower().endswith(supported_cv2_formats)]
        images.sort()

    return images

## https://pytorch.org/tutorials/advanced/super_resolution_with_onnxruntime.html
 
def to_numpy(tensor):
    return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

model = onnx.load("/home/adlink/inference/weights/efficientnet-b5-applescab.onnx")
engine = backend.prepare(model, device='CUDA')

classes = ['healthy','multiple_diseases','rust','scab']
imgdir = '/home/adlink/yolov3_optima/data/apple_scab_20200925/images'
images = list_images(imgdir)

for i in range(len(images)):
    imgname = images[i]
    start_time = time.time()

    # Preprocess image
    tfms = transforms.Compose([transforms.Resize((545, 545)), transforms.ToTensor(), transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),])
    img = tfms(Image.open(os.path.join(imgdir, imgname))).unsqueeze(0)

    # Extract the outputs
    outputs = engine.run(to_numpy(img))[0]
    outputs = torch.tensor(outputs)
    confidences = torch.softmax(outputs, dim=1)

    # Process and print the outputs
    confidences = to_numpy(confidences)
    max_conf_idx = np.argmax(confidences)
    end_time = time.time()
    elapsed_time = end_time-start_time

    # Print the outputs
    print("Image: {0:s}, predicted class: {1:s}, confidence: {2:.2f}, time: {3:.3f}".format(imgname, classes[max_conf_idx], confidences[0][max_conf_idx], elapsed_time))
