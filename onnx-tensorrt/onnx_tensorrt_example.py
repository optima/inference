import tensorrt
import onnx
import onnx_tensorrt.backend as backend
import numpy as np

model = onnx.load("/home/xavieroptima1/yolov3_optima/weights/efficientnet-b5-applescab.onnx")
engine = backend.prepare(model, device='CUDA')
input_data = np.random.random(size=(1, 3, 545, 545)).astype(np.float32)
output_data = engine.run(input_data)[0]
print(output_data)
print(output_data.shape)