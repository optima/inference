from threading import Thread
from time import sleep
from glob import glob
import os

from shutil import move
from Agenso_api import *
from data_types import *

SEND_INTERVAL = 300 #sleep 300secs after transmitting



# only sends the data in processed folder
class send_AGENSO_data(Thread):
    def __init__(self, CONFIG):
        self._running = True
        self.agenso = AGENSO() 
        self.msg_folder = CONFIG['AGENSO-messages-folder']
        self.error_folder = CONFIG['AGENSO-error-folder']
        self.send_interval = CONFIG['AGENSO-send-interval']
        # Initialize the thread
        Thread.__init__(self)
        print("send_data thread started")

    def run(self):
        while self._running:            
            self._upload_files()
            sleep(self.send_interval) #secs

    def _upload_files(self):
        files_to_upload = glob(os.path.join(self.msg_folder, '*.txt'))

        for file_path in files_to_upload :
            data_batch, status = batch_fromfile(file_path)
            if status:
                self.send_batch(data_batch, file_path)
            else:
                error_filepath, ext = self.get_base_filepath(file_path)
                move(file_path, error_filepath)


    def get_base_filepath(self, file_path):
        base_filename, ext = os.path.splitext(os.path.basename(file_path))
        error_filepath = os.path.join(self.error_folder, base_filename)
        return (error_filepath, ext)

    def send_batch(self, data_batch, file_path):
        error_filepath, ext = self.get_base_filepath(file_path)

        data_json = json.dumps({"items":data_batch})
        (upl_status, reply) = self.agenso.batch_upload(data_json)

        if upl_status == True:
            for i in range(len(reply)):#check all the request responces
                if (reply[i][0] == False) or (reply[i][1] != 200):
                    #fautive message posts
                    print("bad request send to Agenso:")
                    print(reply[i][0])
                    print(data_batch[i])
                    fautive_message = data_batch[i]
                    f_file_path = error_filepath + "_" + str(i) + ext
                    data_tofile(fautive_message, f_file_path)
            os.remove(file_path)

    def force_upload(self):
        self._upload_files()
        self._running = False           