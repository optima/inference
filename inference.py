import os
import argparse
import cv2
#import sys
import logging
import yaml

from sys import platform, exit
from datetime import datetime
from threading import Thread, Lock #remove lock > goes to gloval_vars
from queue import Queue
from time import sleep
from glob import glob

from global_vars import *
from camera import *
from Agenso_thread import *
from GPS_receiver import *

## libraries for the deep learning
#mport numpy as np
import onnx
import onnx_tensorrt.backend as backend
import onnxruntime
import torch
import torchvision.transforms as transforms
#import matplotlib
#import matplotlib.pyplot as plt
from PIL import Image, ImageDraw, ImageFont, ImageTk
from utils.onnx_inference import *
import tkinter as tki
import time


class dl_detection(Thread):
    def __init__(self, CONFIG):
        # Initialize the thread
        self._running = True
        self.freememory = free_gbs()
        self.call_counter = 0 
        Thread.__init__(self)

        # Initialize the class specific parameters
        self.classes = [CONFIG['use-case']]


        all_weights = glob('{0:s}/*.{1:s}'.format(os.path.join(CONFIG['weights-folder'], CONFIG['use-case']), 'onnx'))

        if len(all_weights) > 0:
            idx = [idx for idx, s in enumerate(all_weights) if 'yolo' in s][0]
            weightsfile = all_weights[idx]
            if len(weightsfile) > 0:

                # Initialize model
                if not CONFIG['tensorrt']:
                    self.session = onnxruntime.InferenceSession(weightsfile)
                    self.batch_size = self.session.get_inputs()[0].shape[0]
                    self.img_size_h = self.session.get_inputs()[0].shape[2]
                    self.img_size_w = self.session.get_inputs()[0].shape[3]
                else:
                    self.batch_size = 1
                    self.img_size_h = CONFIG['img-size']
                    self.img_size_w = CONFIG['img-size']
                    self.model = onnx.load(weightsfile)
                    self.engine = backend.prepare(self.model, device=CONFIG['device'])
                
                # Initialize the anchors and some other variables
                self.anchors = [[10, 13, 16, 30, 33, 23], [30, 61, 62, 45, 59, 119], [116, 90, 156, 198, 373, 326]]  # 5s
                self.num_classes = len(self.classes)

                # global breakout
                self.breakout = False
                self.max_height = 1200
                self.max_width = 1200
                print("Deep learning thread started")

            else:
                raise SystemExit("No detection weights found in folder: {0:s}\r\nFirst insert the yolov5 weights (.onnx) in folder: {1:s}".format(os.path.join(CONFIG['weights-folder'], CONFIG['use-case']), os.path.join(CONFIG['weights-folder'], CONFIG['use-case'])))
        
        else:
            raise SystemExit("No detection weights found in folder: {0:s}\r\nFirst insert the yolov5 weights (.onnx) in folder: {1:s}".format(os.path.join(CONFIG['weights-folder'], CONFIG['use-case']), os.path.join(CONFIG['weights-folder'], CONFIG['use-case'])))

    def run(self):
        global CONFIG
        while self._running:
            with torch.no_grad():
                detected_classes = []
                bounding_boxes = []
                confidence_scores = []

                if CONFIG['Camera']:
                    while  not to_process_deque:
                        sleep(0.01) #sleep 10 mili seconds
                    item = to_process_deque.pop()
                else:
                    item = to_process_queue.get()
                base_filename = item['file_name']
                img = item['img']
                data_obj = item['data']

                # Preprocess the image
                t = datetime.utcnow()

                # input
                resized = letterbox_image(Image.fromarray(img), (self.img_size_w, self.img_size_h))
                img_in = np.transpose(resized, (2, 0, 1)).astype(np.float32)  # HWC -> CHW
                img_in = np.expand_dims(img_in, axis=0)
                img_in /= 255.0

                # inference
                if not CONFIG['tensorrt']:
                    input_name = self.session.get_inputs()[0].name
                    outputs = self.session.run(None, {input_name: img_in})
                else:
                    img_in = np.array(img_in, dtype=img_in.dtype, order='C')
                    outputs = self.engine.run(img_in)

                batch_detections = []
                boxs = []
                a = torch.tensor(self.anchors).float().view(3, -1, 2)
                anchor_grid = a.clone().view(3, 1, -1, 1, 1, 2)
                if len(outputs) == 4:
                    outputs = [outputs[1], outputs[2], outputs[3]]
                for index, out in enumerate(outputs):
                    out = torch.from_numpy(out)
                    batch = out.shape[1]
                    feature_w = out.shape[2]
                    feature_h = out.shape[3]

                    # Feature map corresponds to the original image zoom factor
                    stride_w = int(self.img_size_w / feature_w)
                    stride_h = int(self.img_size_h / feature_h)
                    grid_x, grid_y = np.meshgrid(np.arange(feature_w), np.arange(feature_h))

                    # cx, cy, w, h
                    pred_boxes = torch.FloatTensor(out[..., :4].shape)
                    pred_boxes[..., 0] = (torch.sigmoid(out[..., 0]) * 2.0 - 0.5 + grid_x) * stride_w  # cx
                    pred_boxes[..., 1] = (torch.sigmoid(out[..., 1]) * 2.0 - 0.5 + grid_y) * stride_h  # cy
                    pred_boxes[..., 2:4] = (torch.sigmoid(out[..., 2:4]) * 2) ** 2 * anchor_grid[index]  # wh

                    conf = torch.sigmoid(out[..., 4])
                    pred_cls = torch.sigmoid(out[..., 5:])

                    output = torch.cat((pred_boxes.view(self.batch_size, -1, 4),
                                        conf.view(self.batch_size, -1, 1),
                                        pred_cls.view(self.batch_size, -1, self.num_classes)),
                                    -1)
                    boxs.append(output)

                outputx = torch.cat(boxs, 1)

                # NMS
                detections = w_non_max_suppression(outputx, self.num_classes, conf_thres= CONFIG['conf-thres'], nms_thres=CONFIG['nms-thres'])
                image_analysis_time = (datetime.utcnow() - t).total_seconds()

                if detections[0] is not None:
                    bounding_boxes = detections[0][..., :4]
                    bounding_boxes = bounding_boxes.int().tolist()

                    confidence_scores = detections[0][..., 4].numpy()
                    confidence_scores = list(np.around(confidence_scores,3))

                    class_ids = detections[0][..., -1] 
                    class_ids = class_ids.int().tolist()
                    for i in range(len(class_ids)):
                        class_id = class_ids[i]
                        detected_classes.append(self.classes[class_id])

                    im0 = display(self.classes, detections[0], img, text_bg_alpha=0.6)
                else:
                    im0 = img

                #print(detected_classes, bounding_boxes, confidence_scores)
                if CONFIG['nn-output-folder'] != None: #store the results
                    self.call_counter = self.call_counter + 1
                    if self.call_counter >= CHECK_EVERY:
                        self.freememory = free_gbs()

                    #only store file if there is enough space
                    if self.freememory > MIN_FREE_SPACE:
                        with open(os.path.join(CONFIG['nn-output-folder'], base_filename + '.txt'), 'w') as file:
                            for i in range(len(detected_classes)):
                                file.write(('%s, %s, %s \r\n') % (detected_classes[i], bounding_boxes[i], confidence_scores[i]))

                #store the avg_confidence & hits for all disease types.
                type_of_diseases = list(set(detected_classes))
                avg_prob = 0
                total_hits = 0
                if len(type_of_diseases) > 0:
                    for dis_type in type_of_diseases:
                        #find indexes and take these indexes from confidence_scores
                        conf_sc_dist = []
                        for ind, val in enumerate(detected_classes):
                            if val == dis_type:
                                conf_sc_dist.append(confidence_scores[ind])
                        avg_conf = float('%.2f' % float(sum(conf_sc_dist)/len(conf_sc_dist)))
                        dis = disease(prob = avg_conf, hits = len(conf_sc_dist))
                        data_obj.diseases.append({dis_type: dis})
                        avg_prob = avg_conf
                        total_hits = len(conf_sc_dist)
                    detected_class = dis_type
                else:                    
                    for dis_type in self.classes:
                        dis = disease(prob = 0, hits = 0)
                        data_obj.diseases.append({dis_type: dis})
                    detected_class = 'healthy'

                #print(data_tojson(data_obj))
                to_send_queue.put({'file_name': base_filename, 'data': data_obj})
                to_display_img.append({'img': im0, 'time': image_analysis_time, 'class': detected_class, 'hits': total_hits, 'prob': avg_prob})


    def terminate(self):
        self._running = False



class dl_classification(Thread):
    def __init__(self, opt):
        # Initialize the thread
        self._running = True
        self.freememory = free_gbs()
        self.call_counter = 0 
        Thread.__init__(self)

        # Initialize the class specific parameters
        self.classes = ['healthy', CONFIG['use-case']]

        all_weights = glob('{0:s}/*.{1:s}'.format(os.path.join(CONFIG['weights-folder'], CONFIG['use-case']), 'onnx'))

        if len(all_weights) > 0:
            if CONFIG['img-patches'] == False:
                idx = [idx for idx, s in enumerate(all_weights) if 'efficientnet_bs1' in s][0]
            else:
                idx = [idx for idx, s in enumerate(all_weights) if 'efficientnet_bs6' in s][0]

            weightsfile = all_weights[idx]
            if len(weightsfile) > 0:

                # Initialize model
                if not CONFIG['tensorrt']:
                    self.sess_options = onnxruntime.SessionOptions()
                    self.session = onnxruntime.InferenceSession(weightsfile, self.sess_options)
                else:
                    self.model = onnx.load(weightsfile)
                    self.engine = backend.prepare(self.model, device=CONFIG['device'])
                
                # Initialize some parameters for the image visualization
                self.max_height = 1200
                self.max_width = 1200
                self.font_face = cv2.FONT_HERSHEY_DUPLEX
                self.font_scale = 2
                self.font_thickness = 2
                self.text_color = [0, 0, 0]
                self.rect_color = [255, 255, 255]
                self.tx1 = 10
                self.ty1 = 50
                self.text_pt = (self.tx1, self.ty1 - 3)

                self.tfms = transforms.Compose([transforms.Resize(int(round(CONFIG['img-size'] * 1.1))),
                                                transforms.CenterCrop(CONFIG['img-size']), 
                                                transforms.ToTensor(), 
                                                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
                                                ])

                ## the x in the lambda functions below represents the PIL-image
                self.patch_tfms = transforms.Compose([transforms.Lambda(lambda x: transforms.Resize(int(round(CONFIG['img-size'] * 1.1)))(x) if min(x.size) < int(round(CONFIG['img-size'] * 1.1)) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((CONFIG['max-img-height'], CONFIG['max-img-width']))(x) if x.size[0] > x.size[1] and (x.size[0] > CONFIG['max-img-width'] or x.size[1] > CONFIG['max-img-height']) else x),
                                                    transforms.Lambda(lambda x: transforms.Resize((CONFIG['max-img-width'], CONFIG['max-img-height']))(x) if x.size[1] > x.size[0] and (x.size[0] > CONFIG['max-img-width'] or x.size[1] > CONFIG['max-img-height']) else x),
                                                    transforms.FiveCrop((CONFIG['img-size'], CONFIG['img-size'])), # this is a list of PIL Images
                                                    transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),
                                                    transforms.Lambda(lambda tensors:
                                                    torch.stack([transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(t) for t in tensors]))])


                print("Deep learning thread started")

            else:
                raise SystemExit("No classification weights found in folder: {0:s}\r\nFirst insert the efficientnet weights (.onnx) in folder: {1:s}".format(os.path.join(CONFIG['weights-folder'], CONFIG['use-case']), os.path.join(CONFIG['weights-folder'], CONFIG['use-case'])))

        else:
            raise SystemExit("No classification weights found in folder: {0:s}\r\nFirst insert the efficientnet weights (.onnx) in folder: {1:s}".format(os.path.join(CONFIG['weights-folder'], CONFIG['use-case']), os.path.join(CONFIG['weights-folder'], CONFIG['use-case'])))

    def run(self):
        global CONFIG
        while self._running:
            with torch.no_grad():
                detected_class = []
                confidence_score = []

                # Access the image data
                if CONFIG['Camera']:
                    while  not to_process_deque:
                        sleep(0.01) #sleep 10 mili seconds
                    item = to_process_deque.pop()
                else:
                    item = to_process_queue.get()
                base_filename = item['file_name']
                img = item['img']
                im0 = img.copy()
                data_obj = item['data']

                # Preprocess the image
                t = datetime.utcnow()
                height, width = img.shape[:2]
                tensor = self.tfms(Image.fromarray(img.astype('uint8'), 'RGB')).unsqueeze(0)

                if CONFIG['img-patches'] == True:
                    patches = self.patch_tfms(Image.fromarray(img.astype('uint8'), 'RGB'))
                    tensor = torch.cat((tensor, patches), dim=0)

                if CONFIG['tensorrt'] == False:
                    # Infer the image-tensor with the ONNX-runtime
                    ort_inputs = {self.session.get_inputs()[0].name: to_numpy(tensor)}
                    ort_outs = self.session.run(None, ort_inputs)
                    outputs = torch.tensor(ort_outs[0])
                else:
                    outputs = self.engine.run(to_numpy(tensor))[0]
                    outputs = torch.tensor(outputs)

                # Process the outputs
                confidences = torch.softmax(outputs, dim=1)
                confidences = to_numpy(confidences)
                max_conf_idxs = np.argmax(confidences, axis=1)
                confidence_scores = confidences[0][max_conf_idxs]
                detected_classes = [self.classes[idx] for idx in max_conf_idxs]

                if all([x == 'healthy' for x in detected_classes]):
                    detected_class = 'healthy'
                    confidence_score = np.mean(confidence_scores)
                else:
                    remainder = self.classes.copy()
                    remainder.remove('healthy')
                    detected_class = remainder[0]
                    sel_idxs = np.where(np.asarray(detected_classes) == detected_class)[0]
                    confidence_score = np.mean(confidence_scores[sel_idxs])

                image_analysis_time = (datetime.utcnow() - t).total_seconds()

                text_str = "class: {0:s}, conf: {1:.2f}".format(detected_class, confidence_score)
                text_w, text_h = cv2.getTextSize(text_str, self.font_face, self.font_scale, self.font_thickness)[0]

                cv2.rectangle(im0, (self.tx1, self.ty1), (self.tx1 + text_w, self.ty1 - text_h - 4), self.rect_color, -1)
                cv2.putText(im0, text_str, self.text_pt, self.font_face, self.font_scale, self.text_color, self.font_thickness, cv2.LINE_AA)        

                if CONFIG['nn-output-folder'] != None: #store the results
                    self.call_counter = self.call_counter + 1
                    if self.call_counter >= CHECK_EVERY:
                        self.freememory = free_gbs()

                    #only store file if there is enough space
                    if self.freememory > MIN_FREE_SPACE:
                        with open(os.path.join(CONFIG['nn-output-folder'], base_filename + '.txt'), 'w') as file:
                            file.write(('%s, %s \r\n') % (detected_class, confidence_score))

                #store the confidence & hit for the detected disease.
                avg_prob = 0
                total_hits = 0
                if detected_class != "healthy":
                    conf = float('%.2f' % float(confidence_score))
                    dis = disease(prob = conf, hits = 1)
                    remainder = self.classes.copy()
                    remainder.remove('healthy')
                    dis_type = remainder[0]
                    data_obj.diseases.append({dis_type: dis})
                    avg_prob = conf
                    total_hits = 1
                else:                    
                    dis = disease(prob = 0, hits = 0)
                    dis_type = "healthy"
                    data_obj.diseases.append({dis_type: dis})

                # print(data_tojson(data_obj))
                to_send_queue.put({'file_name': base_filename, 'data': data_obj})
                to_display_img.append({'img': im0, 'time': image_analysis_time, 'class': detected_class, 'hits': total_hits, 'prob': avg_prob})


    def terminate(self):
        self._running = False


# Saves the values from the to_send_queue into files, they are later send to AGENSO in bluk
class post_processing(Thread):
    def __init__(self, CONFIG):
        self._running = True
        self.freememory = free_gbs()
        self.call_counter = 0 
        self.batch_size = CONFIG['AGENSO-batchsize']
        self.batch = []
        # Initialize the thread
        Thread.__init__(self)
        print("to_send_queue to file saver started")

    def run(self):
        global CONFIG, CHECK_EVERY, MIN_FREE_SPACE
        while self._running:
            item = to_send_queue.get() #blocking operation
            if CONFIG['AGENSO'] == True:
                data = item['data']
                self.batch.append(data)#items as dicts

                if len(self.batch) >= self.batch_size:
                    #check free memory
                    self.call_counter = self.call_counter + 1
                    if self.call_counter >= CHECK_EVERY:
                        self.freememory = free_gbs()

                    #only store file if there is enough space
                    if self.freememory > MIN_FREE_SPACE:
                        #file_path= os.path.join(CONFIG['AGENSO-messages-folder'], (item['file_name'] + ".txt"))
                        file_path = os.path.join(CONFIG['AGENSO-messages-folder'], (datetime.utcnow().strftime("%y-%m-%dT%H:%M:%S") + ".txt"))
                        batch_tofile(self.batch, file_path)
                        self.batch = []

    def terminate(self):
        self._running = False  
        

class GUI(Thread):
    def __init__(self):
        self._running = True

        self.root = tki.Tk()
        self.root.wm_title("Optima - Early detection system")
        self.root.wm_protocol("WM_DELETE_WINDOW", self.terminate)

        self.root.geometry('1280x800')
        self.root.geometry('+0+0')

        self.max_width = 900
        self.max_height = 900

        label = tki.Label(self.root, text = 'Deep learning output:')
        label.config(font=("courier 10 pitch", 20))
        label.grid(row=0, column=1, sticky=tki.W)

        ## initialize the image-frame
        self.panel = tki.Label(self.root)
        self.panel.grid(row=1, column=1, rowspan=50, columnspan=50, sticky=tki.W)

        label = tki.Label(self.root, text = 'GPS, disk, image info:')
        label.config(font=("courier 10 pitch", 16))
        label.grid(row=0, column=52, sticky=tki.W)

        self.fields = ['Time: {:s}', 'GPS time: {:s}', 'GPS quality: {:s}', 'Latitude: {:.6f}', 'Longitude: {:.6f}', 'Moving: {:s}', 'Hdop: {:.1f}', 'Satellites: {:d}', 'Free mem (Jetson): {:.1f} gb', 'Free mem (hdd): {:.1f} gb', 'Image analysis time: {:.2f}s', 'Detected class: {:s}', 'Hits: {:d}', 'Probability: {:.2f}']
        self.labels=[]
        for idx, field in enumerate(self.fields):
            label = tki.Label(self.root, text=field)
            label.config(font=("courier 10 pitch", 14))
            label.grid(row = (idx+1), column = 52, sticky = tki.W)
            self.labels.append(label)

        self.stopbtn = tki.Button(self.root, text="Stop program", width=16, height=3, compound="c", bg='red', fg='white', command=lambda: Thread(target=self.terminate, args=()).start())
        self.stopbtn.config(font=("courier 10 pitch", 20))
        self.stopbtn.grid(row=50, column=52, sticky=tki.W)

        self.root.grid_rowconfigure(0, minsize=10)
        self.root.grid_columnconfigure(0, minsize=10)
        self.root.grid_columnconfigure(51, minsize=10)

        Thread.__init__(self)
        self.start()
        self.root.mainloop()

    def run(self):
        while self._running:
            self.root.after(100, self.update_values())

        self.root.quit()

    def update_values(self):
        # data
        if to_display_deque:
            item = to_display_deque.popleft()
            data_obj = item['data']
            free_mem = item['free_mem']
            free_mem_hdd = item['free_mem_hdd']
            is_moving = item['is_moving']
            gps_rectime = item['gps_rectime']

            self.labels[0].configure(text=self.fields[0].format(datetime.now().strftime("%H:%M:%S")))
            self.labels[1].configure(text=self.fields[1].format(gps_rectime.split("T")[1]))
            self.labels[2].configure(text=self.fields[2].format(data_obj.gps_quality))
            self.labels[3].configure(text=self.fields[3].format(data_obj.lat))
            self.labels[4].configure(text=self.fields[4].format(data_obj.lng))
            self.labels[5].configure(text=self.fields[5].format(str(is_moving)))
            self.labels[6].configure(text=self.fields[6].format(data_obj.hdop))
            self.labels[7].configure(text=self.fields[7].format(data_obj.satnum))
            self.labels[8].configure(text=self.fields[8].format(free_mem))
            self.labels[9].configure(text=self.fields[9].format(free_mem_hdd))
        
        #image
        if to_display_img:
            img_item = to_display_img.popleft()
            img_obj = img_item['img']
            analysis_time = img_item['time']
            det_class = img_item['class']
            hits = img_item['hits']
            prob = img_item['prob']

            img_rgb = cv2.cvtColor(img_obj, cv2.COLOR_BGR2RGB)
            img_rgb = Image.fromarray(img_rgb)
            img_rgb.thumbnail((self.max_width, self.max_height), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img_rgb)
            self.panel.configure(image=img)
            self.panel.image = img

            self.labels[10].configure(text=self.fields[10].format(analysis_time))
            self.labels[11].configure(text=self.fields[11].format(det_class))
            self.labels[12].configure(text=self.fields[12].format(hits))
            self.labels[13].configure(text=self.fields[13].format(prob))

    def terminate(self):
        self._running = False

    def close_window(self):
        self.root.destroy()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--cfg', type = str, default = '/home/adlink/inference/cfg/detection.yaml', help='YAML with inference parameters')
    args = parser.parse_args()

    # Dont start program when we have less than 100 mb of free space
    freememory = free_gbs()
    if freememory < 0.1:
        print("disk full!, please empty the trashcan before you continue")
        while(True):
            sleep(10)
    

    #read config file
    try:
        with open(args.cfg, 'rb') as file:
            CONFIG =yaml.load(file, Loader = yaml.FullLoader)
    except FileNotFoundError:
        sys.exit(f"could not find settings file ({args.cfg}), closing application")

    #make sure the folders exist
    check_direxcist(CONFIG['nn-output-folder'])
    check_direxcist(CONFIG['AGENSO-messages-folder'])
    check_direxcist(CONFIG['AGENSO-error-folder'])

    #setup logging
    if CONFIG['logging'] == True:
        log_folder = "logs"
        check_direxcist(log_folder)
        logging.basicConfig(level= logging.INFO, format='%(message)s')
        logger = logging.getLogger()
        current_imestamp = datetime.utcnow().strftime("%y-%m-%dT%H:%M:%S")
        logger.addHandler(logging.FileHandler(log_folder + "/log_" + current_imestamp, 'a'))
        print = logger.info

    # Initialize the threads
    if CONFIG['GPS'] == True: #GPS
        start_GPSthread(CONFIG)

    #camera
    if CONFIG['Camera'] == True:#take images
        image_thread = take_img(CONFIG)
    else:
        image_thread = read_img(CONFIG)

    #DL
    if CONFIG['dl-mode'] == 'detection':
        dl_thread = dl_detection(CONFIG)
    elif CONFIG['dl-mode'] == 'classification':
        dl_thread = dl_classification(CONFIG)
    else:
        raise Exception('selected DL mode not recognized!')    

    #grab results from DL thread
    proc_res_thread = post_processing(CONFIG)

    #AGENSO file send thread
    if CONFIG['AGENSO'] == True:
        AGENSO_thread = send_AGENSO_data(CONFIG)

    # Start the threads
    image_thread.start()
    dl_thread.start()
    proc_res_thread.start()
    if CONFIG['AGENSO'] == True:
        AGENSO_thread.start()

    gui = GUI()

    stop_GPSthread()
    image_thread.terminate()
    dl_thread.terminate()
    proc_res_thread.terminate()

    if CONFIG['AGENSO'] == True:
        AGENSO_thread.force_upload()
        wait = 0
        while wait < 10: # give AGENSO max 10 seconds to upload the last data
            if AGENSO_thread._running == False:
                break
            sleep(1)
    # close gui       
    gui.close_window()
    sys.exit()