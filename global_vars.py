from threading import Lock
from collections import deque
from queue import Queue
import shutil
import os


from data_types import *

#GLOBAL VARS

#GPS to img-thread
GPS_lock = Lock()
GPS_ringbuf = GPS_Ringbuffer(15)# save last x values

#img-thread to NN-thread
to_display_deque = deque(maxlen=1)
to_display_img = deque(maxlen=1)
to_process_queue = Queue(maxsize=10) #read img to neural net, item= (filename, img, data)
to_process_deque = deque(maxlen=10) #ringbuffer with 10 latest images

## NN-thread to files...
to_send_queue = Queue(maxsize=20)
CHECK_EVERY = 100 #check the free diskspace every 100 runs
MIN_FREE_SPACE = 0.1 #minimum amount of GBs below which we stop writing to avoid crashes!


def check_direxcist(dir):
    if dir is not None:
        if not os.path.exists(dir):
            os.makedirs(dir)  # make new folder

#check free gbs on the harddisk
def free_gbs():
    #total, used, free = shutil.disk_usage(__file__)
    _, _, free = shutil.disk_usage(__file__)
    return (free / 1000000000)